<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
            <?= $this->session->flashdata('success'); ?>
            <?= $this->session->flashdata('errors'); ?>
        </div>
    </div>

    <!-- card data pembayaran -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Dosen</h4>
                    <a class="btn btn-primary shadow" href="#tambah" data-toggle="modal"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Tambah Dosen</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableIuran">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">NIDN</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($dosen as $value) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->nama ?></td>
                                        <td><?= $value->nidn ?></td>
                                        <td><?= $value->email ?></td>
                                        <td><?= $value->username ?></td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-info mr-1" onclick="edit(this)" data-id="<?= $value->id ?>" data-nama="<?= $value->nama ?>" data-nidn="<?= $value->nidn ?>" data-email="<?= $value->email ?>" data-username="<?= $value->username ?>"><i class="fas fa-edit fa-sm"></i> Edit</a>

                                            <a href="<?=base_url('dosen/dosen/destroy')?>/<?= $value->id; ?>" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');"><i class="fas fa-trash-alt fa-sm"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data pembayaran -->
                                
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data Dosen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('dosen/dosen/store'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Nama Dosen</label>
                                <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Dosen" require>
                            </div>
                            <div class="form-group">
                                <label for="">NIDN</label>
                                <input type="number" name="nidn" class="form-control" placeholder="Masukkan NIDN" require>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Masukkan email" require>
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control" placeholder="Masukkan username" require>
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Masukkan password" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Paket Kelas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" action="<?= base_url('dosen/dosen/update'); ?>">
                    <div class="row">
                        <input type="hidden" name="id" id="id">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Nama Dosen</label>
                                <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Dosen" require>
                            </div>
                            <div class="form-group">
                                <label for="">NIDN</label>
                                <input type="number" name="nidn" id="nidn" class="form-control" placeholder="Masukkan NIDN" require>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Masukkan email" require>
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Masukkan username" require>
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Masukkan password" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<script>
    function edit(el) {
        console.log($(el).data('nidn'));
        $('#edit').modal('show');
        $('#id').val($(el).data('id'));
        $('#nama').val($(el).data('nama'));
        $('#nidn').val($(el).data('nidn'));
        $('#email').val($(el).data('email'));
        $('#username').val($(el).data('username'));
    }
</script>