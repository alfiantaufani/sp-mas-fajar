<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
            <?= $this->session->flashdata('success'); ?>
            <?= $this->session->flashdata('errors'); ?>
        </div>
    </div>

    <!-- card data pembayaran -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Jadwal</h4>
                    <a class="btn btn-primary shadow" href="#tambah" data-toggle="modal"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Tambah Jadwal</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableIuran">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Kode MK</th>
                                    <th scope="col">Matakuliah</th>
                                    <th scope="col">SKS</th>
                                    <th scope="col">Jadwal</th>
                                    <th scope="col">Kelas</th>
                                    <th scope="col">Dosen</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($jadwal as $value) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->kode_matkul ?></td>
                                        <td><?= $value->nama_matkul ?></td>
                                        <td><?= $value->sks_matkul ?></td>
                                        <td><?= $value->hari ?>, <?= $value->jam_ke ?> (<?= $value->jam_mulai ?>-<?= $value->jam_selesai ?>)</td>
                                        <td><?= $value->kelas ?></td>
                                        <td><?= $value->nama ?></td>
                                        <td>
                                            <a href="#edit<?= $value->id_jadwal; ?>" data-toggle="modal" class="btn btn-primary"><i class="fas fa-edit fa-sm"></i> Edit</a>

                                            <a href="<?=base_url('dosen/jadwal/destroy')?>/<?= $value->id_jadwal; ?>" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');"><i class="fas fa-trash-alt fa-sm"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data pembayaran -->
                                
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data Jadwal</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('dosen/jadwal/store'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Matakuliah</label>
                                <select name="id_matkul" class="form-control">
                                    <option selected disabled>--Pilih Matakuliah--</option>
                                    <?php foreach ($matkul as $matkuls) :?>
                                        <option value="<?= $matkuls->id?>"><?= $matkuls->nama_matkul?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Dosen</label>
                                <select name="id_dosen" class="form-control">
                                    <option selected disabled>--Pilih Dosen--</option>
                                    <?php foreach ($dosen as $dosens) :?>
                                        <option value="<?= $dosens->id?>"><?= $dosens->nama?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Kelas</label>
                                <select name="id_kelas" class="form-control">
                                    <option selected disabled>--Pilih Kelas--</option>
                                    <?php foreach ($kelas as $kelass) :?>
                                        <option value="<?= $kelass->id?>"><?= $kelass->kelas?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Hari</label>
                                <select name="hari" class="form-control">
                                    <option selected disabled>--Pilih hari--</option>
                                    <?php foreach ($hari as $haris) :?>
                                        <option value="<?= $haris['hari']?>"><?= $haris['hari']?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Jam Ke</label>
                                <select name="jam_ke" class="form-control">
                                    <option selected disabled>--Pilih Jam Ke--</option>
                                    <?php foreach ($jam_ke as $jam_kes) :?>
                                        <option value="<?= $jam_kes['jam_ke']?>"><?= $jam_kes['jam_ke']?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Jam Mulai</label>
                                <input type="time" name="jam_mulai" class="form-control" placeholder="Masukkan Jam Mulai" require>
                            </div>
                            <div class="form-group">
                                <label for="">Jam Selesai</label>
                                <input type="time" name="jam_selesai" class="form-control" placeholder="Masukkan Jam Selesai" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<!-- tambah Pembayaran Modal-->
<?php foreach ($jadwal as $jadwals) : ?>
    <div class="modal fade" id="edit<?= $jadwals->id_jadwal; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Jadwal</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <form method="post" action="<?= base_url('dosen/jadwal/update'); ?>">
                        <div class="row">
                            <input type="hidden" name="id" value="<?= $jadwals->id_jadwal ?>">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label for="">Matakuliah</label>
                                    <select name="id_matkul" class="form-control">
                                        <?php foreach ($matkul as $matkuls) :?>
                                            <?php if($jadwals->id_matkul == $matkuls->id) : ?>
                                                <option value="<?= $jadwals->id_matkul?>" selected><?= $jadwals->nama_matkul?></option>
                                            <?php else : ?>
                                                <option value="<?= $matkuls->id?>"><?= $matkuls->nama_matkul?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Dosen</label>
                                    <select name="id_dosen" class="form-control">
                                        <?php foreach ($dosen as $dosens) :?>
                                            <?php if($jadwals->id_dosen == $dosens->id) : ?>
                                                <option value="<?= $jadwals->id_dosen?>" selected><?= $jadwals->nama?></option>
                                            <?php else : ?>
                                                <option value="<?= $dosens->id?>"><?= $dosens->nama?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Kelas</label>
                                    <select name="id_kelas" class="form-control">
                                        <?php foreach ($kelas as $kelass) :?>
                                            <?php if($jadwals->id_kelas == $kelass->id) : ?>
                                                <option value="<?= $jadwals->id_kelas?>" selected><?= $jadwals->kelas?></option>
                                            <?php else : ?>
                                                <option value="<?= $kelass->id?>"><?= $kelass->kelas?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Hari</label>
                                    <select name="hari" class="form-control">
                                        <?php foreach ($hari as $days) :?>
                                            <?php if($jadwals->hari == $days['hari']) : ?>
                                                <option value="<?= $jadwals->hari?>" selected><?= $jadwals->hari?></option>
                                            <?php else : ?>
                                                <option value="<?= $days['hari']?>"><?= $days['hari'] ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Jam Ke</label>
                                    <select name="jam_ke" class="form-control">
                                        <?php foreach ($jam_ke as $jamke) :?>
                                            <?php if($jadwals->jam_ke == $jamke['jam_ke']) : ?>
                                                <option value="<?= $jadwals->jam_ke?>" selected><?= $jadwals->jam_ke?></option>
                                            <?php else : ?>
                                                <option value="<?= $jamke['jam_ke']?>"><?= $jamke['jam_ke'] ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Jam Mulai</label>
                                    <input type="time" name="jam_mulai" class="form-control" placeholder="Masukkan Jam Mulai" value="<?= $jadwals->jam_mulai?>" require>
                                </div>
                                <div class="form-group">
                                    <label for="">Jam Selesai</label>
                                    <input type="time" name="jam_selesai" class="form-control" placeholder="Masukkan Jam Selesai" value="<?= $jadwals->jam_selesai?>" require>
                                </div>
                                
                                <div class="form-group float-right">
                                    <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- akhir form input -->

                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>
<!-- /.akhir tambah pembayaran Modal -->

<script>
    function edit(el) {
        $('#edit').modal('show');
        $('#id').val($(el).data('id'));
        $('#nama').val($(el).data('nama'));
        $('#nim').val($(el).data('nim'));
        $('#email').val($(el).data('email'));
        $('#username').val($(el).data('username'));
    }
</script>