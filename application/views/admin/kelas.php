<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
            <?= $this->session->flashdata('success'); ?>
            <?= $this->session->flashdata('errors'); ?>
        </div>
    </div>

    <!-- card data pembayaran -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Kelas</h4>
                    <a class="btn btn-primary shadow" href="#tambah" data-toggle="modal"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Tambah Kelas</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableIuran">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Kelas</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($kelas as $value) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->kelas ?></td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-info mr-1" onclick="edit(this)" data-id="<?= $value->id ?>" data-kelas="<?= $value->kelas ?>"><i class="fas fa-edit fa-sm"></i> Edit</a>

                                            <a href="<?=base_url('dosen/kelas/destroy')?>/<?= $value->id; ?>" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');"><i class="fas fa-trash-alt fa-sm"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data pembayaran -->
                                
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data Kelas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('dosen/kelas/store'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Nama Kelas</label>
                                <input type="text" name="kelas" class="form-control" placeholder="Masukkan Nama Kelas" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Kelas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" action="<?= base_url('dosen/kelas/update'); ?>">
                    <div class="row">
                        <input type="hidden" name="id" id="id">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Nama Kelas</label>
                                <input type="text" name="kelas" id="kelas" class="form-control" placeholder="Masukkan Nama Kelas" require>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<script>
    function edit(el) {
        $('#edit').modal('show');
        $('#id').val($(el).data('id'));
        $('#kelas').val($(el).data('kelas'));
    }
</script>