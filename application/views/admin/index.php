<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?= $title; ?></h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="text-center">
                                <img class="img-fluid px-3 px-sm-4 mb-2" style="width: 25rem;"
                                    src="<?php echo base_url('assets/img/undraw_posting_photo.svg'); ?>" alt="...">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h3><b>Selamat datang Dosen</b></h3>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->