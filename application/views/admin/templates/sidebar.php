<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('user'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-user-graduate"></i>
        </div>
        <div class="sidebar-brand-text mx-3">E-<small><em>Jadwal</em></small></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
        <div class="sidebar-heading">


        </div>
                <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('dosen/dashboard') ?>">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'mata-kuliah' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('dosen/mata-kuliah') ?>">
                        <i class="fas fa-fw fa-database"></i>
                        <span>Mata Kuliah</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'jadwal' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('dosen/jadwal') ?>">
                        <i class="fas fa-fw fa-users"></i>
                        <span>Jadwal</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'kelas' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('dosen/kelas') ?>">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                        <span>Kelas</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'mahasiswa' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('dosen/mahasiswa') ?>">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                        <span>Mahasiswa</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'dosen' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('dosen/dosen') ?>">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                        <span>Dosen</span>
                    </a>
                </li>
            <!-- Divider -->
            <hr class="sidebar-divider mt-3">

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link btn-logout" href="<?= base_url('auth/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-fw fa-power-off"></i>
                <span>Logout</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

</ul>
<!-- End of Sidebar -->