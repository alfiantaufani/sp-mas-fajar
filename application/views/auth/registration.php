<style>
  label{
    font-weight: 600;
  }
</style>
<div class="container">

  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="container">
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block text-center">
            <img src="<?= base_url('assets/'); ?>logo/siap-bayar-kecil.png" alt="logo-image" class="img-circle mt-5 mb-3">
            <h1 class="h4 text-gray-900 mb-1"><b>AHE</b>-<em>Bimbel</em></h1>
            <p class="mb-5"><em class="text-primary">Sistem Informasi Bimbingan Belajar Ahe Morosunggingan</em></p>
            <h1 class="h4 lead text-gray-900"><i>Buruan Daftar Sekarang! GRATIS...</i></h1>
          </div>
          <div class="col-lg-7">
            <div class="p-4">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-5 mt-5">Formulir Pendfataran Siswa Baru</h1>
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('message_email'); ?>
              </div>
              <form class="user" method="post" action="<?= base_url('auth/register'); ?>">
                <div class="form-group">
                  <label for="">Nama Siswa</label>
                  <input type="text" class="form-control" id="name" name="nama_siswa" placeholder="Masukkan nama anda, contoh : Muhammad Saifullah" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-12 mb-3 mb-sm-0">
                    <label for="">NIK Siswa</label>
                    <input type="number" class="form-control" id="name" name="nik_siswa" placeholder="Masukkan NIK Siswa" required>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <label for="">Jenis Kelamin</label>
                    <select name="jenis_kelamin" id="" class="form-control" required>
                      <option disabled selected hidden>-- Pilih Jenis Kelamin --</option>
                      <option value="Laki-laki">Laki-laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <label for="">Pilih Paket Kelas</label>
                    <select name="paket_kelas" id="" class="form-control" required>
                      <option disabled selected hidden>-- Pilih Paket Kelas --</option>
                      <?php foreach ($kelas as $data) :?>
                          <option value="<?= $data['id'] ?>"><?= $data['nama'] ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="">Nama Ayah</label>
                  <input type="text" class="form-control" id="name" name="nama_ayah" placeholder="Masukkan Ayah" required>
                </div>
                <div class="form-group">
                  <label for="">Nama Ibu</label>
                  <input type="text" class="form-control" id="name" name="nama_ibu" placeholder="Masukkan Ibu" required>
                </div>
                <div class="form-group">
                  <label for="">No. Telepon Orang Tua</label>
                  <input type="text" class="form-control" id="name" name="no_hp_ortu" placeholder="Masukkan No. Telepon yang bisa dihubungi" required>
                </div>
                <div class="form-group">
                  <label for="">Alamat Orang Tua</label>
                  <textarea class="form-control" name="alamat_ortu" id="" cols="30" rows="2" placeholder="Masukkan Alamat Orang Tua" required></textarea>
                </div>
                <div class="form-group">
                  <label for="">Email</label>
                  <input type="email" class="form-control" name="email" placeholder="Masukkan Email" required>
                </div>
                
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <label for="">Password</label>
                    <input type="password" class="form-control" id="password1" name="password1" placeholder="Masukkan Password" required>
                  </div>
                  <div class="col-sm-6">
                    <label for="">Ulangi Password</label>
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="Ulangi Password" required>
                  </div>
                </div>

                <button type="submit" class="btn btn-primary btn-user btn-block">
                  Daftar Sekarang
                </button>
              </form>
              <hr>
              <div class="text-center">
                <!-- <a class="small" href="<?= base_url('auth/forgotpassword'); ?>">Forgot Password?</a> -->
              </div>
              <div class="text-center">
                Apakah anda sudah terdaftar? <a class="small" href="<?= base_url('auth') ?>">Silahkan Login disini!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>