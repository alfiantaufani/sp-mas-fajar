<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mtransaksi extends CI_Model
{
    public function tampil_tagihan_siswa()
    {
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->group_by('data_siswa.id_siswa');
        return $this->db->get()->result_array();
    }

    public function data_siswa($id_siswa)
    {
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->group_by('tagihan.id_siswa');
        $this->db->where('tagihan.id_siswa', $id_siswa);
        return $this->db->get()->result_array();
    }

    public function tampil_tagihan_siswa_id($id_siswa)
    {
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        //$this->db->group_by('tagihan.id_siswa');
        $this->db->where('tagihan.id_siswa', $id_siswa);
        return $this->db->get()->result_array();
    }

    public function tampil_tagihan_pending($id_siswa)
    {
        $pending = "pending";
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->where('tagihan.id_siswa', $id_siswa);
        $this->db->where('tagihan.status', $pending);
        return $this->db->get()->result_array();
    }

    public function tampil_tagihan_lunas($id_siswa)
    {
        $lunas = "lunas";
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->where('tagihan.id_siswa', $id_siswa);
        $this->db->where('tagihan.status', $lunas);
        return $this->db->get()->result_array();
    }

    public function tampil_transaksi($id_siswa)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('data_siswa', 'transaksi.id_siswa = data_siswa.id_siswa');
        $this->db->join('tagihan', 'transaksi.tagihan_id = tagihan.id_tagihan');
        $this->db->join('pembayaran', 'tagihan.id_pembayaran = pembayaran.id');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');

        $this->db->where('transaksi.id_siswa',$id_siswa);
        $result= $this->db->get()->result_array();
		return $result;  
    }

    public function tampil_transaksi_print($id_transaksi)
    {
        $this->db->select('*');
		$this->db->from('transaksi');
        //$this->db->join('data_siswa','transaksi.id_siswa=data_siswa.id_siswa');
        //$this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->where('id', $id_transaksi);
        return $this->db->get()->row_array();
    }
}