<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('dosen_masuk') != TRUE){
			redirect('auth');
		}
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $this->db->select('*');
        $this->db->from('mahasiswa');
        $this->db->join('kelas', 'mahasiswa.id_kelas=kelas.id');
        $mahasiswa = $this->db->get();

        $data['mahasiswa'] = $mahasiswa->result();
        $data['kelas'] = $this->db->get('kelas')->result();

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/templates/sidebar', $data);
        $this->load->view('admin/templates/topbar', $data);
        $this->load->view('admin/mahasiswa', $data);
        $this->load->view('admin/templates/footer', $data);
    }

    public function store()
    {
        $cek_username = $this->db->get_where('mahasiswa', array('username' => $this->input->post('username')));
        if ($cek_username->num_rows() > 0) {
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Username sudah digunakan</div>');
            redirect('dosen/mahasiswa');
        }
        $data = [
            'nama' => $this->input->post('nama'),
            'nim' => $this->input->post('nim'),
            'id_kelas' => $this->input->post('id_kelas'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
        ];

        $insert = $this->db->insert('mahasiswa', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah mahasiswa</div>');
            redirect('dosen/mahasiswa');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah mahasiswa</div>');
            redirect('dosen/mahasiswa');
        }
    }

    public function destroy($id)
    {
        $delete = $this->db->delete('mahasiswa', array('id' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus mahasiswa</div>');
            redirect('dosen/mahasiswa');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus mahasiswa</div>');
            redirect('dosen/mahasiswa');
        }
    }

    public function update()
    {
        if ($this->input->post('password') == "") {
            if ($this->input->post('id_kelas') == "") {
                $data = [
                    'nama' => $this->input->post('nama'),
                    'nim' => $this->input->post('nim'),
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                ];
            }else{
                $data = [
                    'nama' => $this->input->post('nama'),
                    'nim' => $this->input->post('nim'),
                    'id_kelas' => $this->input->post('id_kelas'),
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                ];
            }
        }else{
            if ($this->input->post('id_kelas') == "") {
                $data = [
                    'nama' => $this->input->post('nama'),
                    'nim' => $this->input->post('nim'),
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                ];
            }else{
                $data = [
                    'nama' => $this->input->post('nama'),
                    'nim' => $this->input->post('nim'),
                    'id_kelas' => $this->input->post('id_kelas'),
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password'),
                ];
            }
        }
        
        $this->db->where('id', $this->input->post('id'));
        $update = $this->db->update('mahasiswa', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah mahasiswa</div>');
            redirect('dosen/mahasiswa');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah mahasiswa</div>');
            redirect('dosen/mahasiswa');
        }
    }
}
