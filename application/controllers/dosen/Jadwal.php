<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('dosen_masuk') != TRUE){
			redirect('auth');
		}
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $this->db->select('*');
        $this->db->from('jadwal');
        $this->db->join('mata_kuliah', 'jadwal.id_matkul=mata_kuliah.id');
        $this->db->join('dosen', 'jadwal.id_dosen=dosen.id');
        $this->db->join('kelas', 'jadwal.id_kelas=kelas.id');
        $jadwal = $this->db->get();

        $data['jadwal'] = $jadwal->result();
        $data['matkul'] = $this->db->get('mata_kuliah')->result();
        $data['dosen'] = $this->db->get('dosen')->result();
        $data['kelas'] = $this->db->get('kelas')->result();
        $data['hari'] = $this->hari();
        $data['jam_ke'] = $this->jam_ke();
        // echo json_encode($data['jadwal']);
        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/templates/sidebar', $data);
        $this->load->view('admin/templates/topbar', $data);
        $this->load->view('admin/jadwal', $data);
        $this->load->view('admin/templates/footer', $data);
    }

    public function store()
    {
        $data = [
            'id_matkul' => $this->input->post('id_matkul'),
            'id_dosen' => $this->input->post('id_dosen'),
            'id_kelas' => $this->input->post('id_kelas'),
            'id_ketentuan' => '1',
            'hari' => $this->input->post('hari'),
            'jam_ke' => $this->input->post('jam_ke'),
            'jam_mulai' => $this->input->post('jam_mulai'),
            'jam_selesai' => $this->input->post('jam_selesai'),
        ];

        $insert = $this->db->insert('jadwal', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah Jadwal</div>');
            redirect('dosen/jadwal');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah Jadwal</div>');
            redirect('dosen/jadwal');
        }
    }

    public function destroy($id)
    {
        $delete = $this->db->delete('jadwal', array('id_jadwal' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus Jadwal</div>');
            redirect('dosen/jadwal');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus Jadwal</div>');
            redirect('dosen/jadwal');
        }
    }

    public function update()
    {
        $data = [
            'id_matkul' => $this->input->post('id_matkul'),
            'id_dosen' => $this->input->post('id_dosen'),
            'id_kelas' => $this->input->post('id_kelas'),
            'hari' => $this->input->post('hari'),
            'jam_ke' => $this->input->post('jam_ke'),
            'jam_mulai' => $this->input->post('jam_mulai'),
            'jam_selesai' => $this->input->post('jam_selesai'),
        ];

        $this->db->where('id_jadwal', $this->input->post('id'));
        $update = $this->db->update('jadwal', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah Jadwal</div>');
            redirect('dosen/jadwal');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah Jadwal</div>');
            redirect('dosen/jadwal');
        }
    }

    public function hari()
    {
        $hari = [
            [
                'hari' => 'Senin',
            ],
            [
                'hari' => 'Selasa',
            ],
            [
                'hari' => 'Rabu',
            ],
            [
                'hari' => 'Kamis',
            ],
            [
                'hari' => "Jum'at",
            ],
            [
                'hari' => "Sabtu",
            ],
            [
                'hari' => "Ahad",
            ],
        ];
        return $hari;
    }

    public function jam_ke()
    {
        $hari = [
            [
                'jam_ke' => 'I',
            ],
            [
                'jam_ke' => 'II',
            ],
            [
                'jam_ke' => 'III',
            ],
            [
                'jam_ke' => 'IV',
            ],
            [
                'jam_ke' => "V",
            ],
            [
                'jam_ke' => "VI",
            ]
        ];
        return $hari;
    }
}
