<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('dosen_masuk') != TRUE){
			redirect('auth');
		}
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('dosen', ['id' => $this->session->userdata('dosen_id')])->row_array();

        $data['kelas'] = $this->db->get('kelas')->result();

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/templates/sidebar', $data);
        $this->load->view('admin/templates/topbar', $data);
        $this->load->view('admin/kelas', $data);
        $this->load->view('admin/templates/footer', $data);
    }

    public function store()
    {
        $data = [
            'kelas' => $this->input->post('kelas'),
        ];

        $insert = $this->db->insert('kelas', $data);
        if ($insert) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil tambah data kelas</div>');
            redirect('dosen/kelas');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal tambah data kelas</div>');
            redirect('dosen/kelas');
        }
    }

    public function destroy($id)
    {
        $delete = $this->db->delete('kelas', array('id' => $id));
        if ($delete) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil hapus data kelas</div>');
            redirect('dosen/kelas');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal hapus data kelas</div>');
            redirect('dosen/kelas');
        }
    }

    public function update()
    {
        $data = [
            'kelas' => $this->input->post('kelas'),
        ];

        $this->db->where('id', $this->input->post('id'));
        $update = $this->db->update('kelas', $data);
        if ($update) {
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Berhasil ubah data kelas</div>');
            redirect('dosen/kelas');
        }else{
            $this->session->set_flashdata('errors', '<div class="alert alert-danger" role="alert">Gagal ubah data kelas</div>');
            redirect('dosen/kelas');
        }
    }
}
