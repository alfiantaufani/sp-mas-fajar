<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('username') != FALSE) {
            redirect('user');
        }

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'SIAP-bayar Login';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            // jika validasinya success
            $this->_login();
        }
    }

    private function _login()
    {
        $username      = htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);//ambil form
        $password   = htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);//ambil form

        //selek di tbl user
        $admin = $this->db->query("SELECT * FROM dosen WHERE username='$username' AND password=MD5('$password') LIMIT 1");

        //$userWalikelas = $this->db->get_where('walikelas', ['username' => $username])->row_array();
        
        if($admin->num_rows() > 0){
            $dosen = $admin->row_array();
            
            $this->session->set_userdata('dosen_masuk',TRUE);
            $this->session->set_userdata('dosen_username',$dosen['username']);
            $this->session->set_userdata('dosen_id',$dosen['id']);
            $this->session->set_userdata('dosen_nama',$dosen['nama']);
            redirect('dosen/dashboard');
            
        }else{
            $mahasiswa = $this->db->query("SELECT * FROM mahasiswa WHERE username='$username' AND password = MD5('$password') LIMIT 1");

            if ($mahasiswa->num_rows() > 0) {
                $mahasiswa = $mahasiswa->row_array();

                $this->session->set_userdata('mahasiswa_masuk',TRUE);
                $this->session->set_userdata('mahasiswa_username',$mahasiswa['username']);
                $this->session->set_userdata('mahasiswa_id',$mahasiswa['id']);
                $this->session->set_userdata('mahasiswa_nama',$mahasiswa['nama']);
                redirect('mahasiswa');
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username atau Password anda salah!</div>');
                redirect('auth');
            }
            

        }
    }


    public function logout()
    {
        $this->session->sess_destroy();;
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        anda sudah logged out!</div>
        ');
        redirect('auth');
    }

}
